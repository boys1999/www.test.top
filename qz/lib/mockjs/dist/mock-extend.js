
// 自定义占位数据
Mock.Random.extend({
    headimg: function(date) {
        var headimgs = [
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/TB1eEn9QFXXXXbbaXXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/TB14ohUQFXXXXXWaXXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/TB1VNbYQVXXXXbyXVXXXXXXXXXX_!!2-item_pic.png_640x640.jpg_.webp',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i3/TB1KRbzPFXXXXagXVXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/TB1angNQXXXXXb7XVXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/TB1Kbr8QFXXXXb9XXXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/TB1fsMiRXXXXXXCaXXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/TB1TO6ePVXXXXbxXVXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://img.alicdn.com/imgextra/i4/1995305013405882546/TB2At5ZgipnpuFjSZFkXXc4ZpXa_!!0-saturn_solar.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/TB1Ruo6PXXXXXXFXpXXYXGcGpXX_M2.SS2_640x640.jpg_.webp',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/TB1As0oPpXXXXbXaFXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/TB1V6b_QVXXXXcXXpXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i1/TB1szbHHXXXXXX2XVXXXXXXXXXX_!!0-item_pic.jpg_640x640.jpg_.webp',
            'https://g-search1.alicdn.com/img/bao/uploaded/i4/i4/1074906482/TB2WNhgqFXXXXbPXXXXXXXXXXXX_!!1074906482.jpg_640x640.jpg_.webp',
            'https://img.alicdn.com/imgextra/i4/1945505012366163577/TB2NvyUeYFlpuFjy0FgXXbRBVXa_!!0-saturn_solar.jpg_640x640.jpg_.webp',
            'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/73198493/TB2x.bZXMxlpuFjSszgXXcJdpXa_!!73198493.jpg_640x640.jpg_.webp',
            'https://img.alicdn.com/imgextra/i3/1455206018914670378/TB2z3zNoodnpuFjSZPhXXbChpXa_!!0-saturn_solar.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/24686092/TB2cVoJlpXXXXXvXpXXXXXXXXXX_!!24686092.jpg_640x640.jpg_.webp',
            'https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp'
        ]
        return this.pick(headimgs)
    }
});