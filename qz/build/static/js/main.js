$(function() {
    // 首页
    $(document).on("pageInit", "#index", function(e, id, page) {
        // 页面数据
        QDD.data.index = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.index;
    });
    // 动态列表
    $(document).on("pageInit", "#nearby_dynamic", function(e, id, page) {
        // 页面数据
        QDD.data.nearby_dynamic = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.nearby_dynamic;

        // 初始化页面数据
        QDD.pageInit(data);
    });
    // 动态详情
    $(document).on("pageInit", "#nearby_dynamic_rate", function(e, id, page) {
        $(page).css({"z-index":"3003" });//顶最顶层
        // 页面数据
        QDD.data.nearby_dynamic_rate = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.nearby_dynamic_rate;

        // 初始化页面数据
        QDD.pageInit(data);
    });
    // 附近的人/找人
    $(document).on("pageInit", "#nearby_people", function(e, id, page) {
        // 页面数据
        QDD.data.nearby_people = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.nearby_people;

        // 初始化页面数据
        QDD.pageInit(data);
    });
    // 个人中心 首页
    $(document).on("pageInit", "#my_index", function(e, id, page) {
        // 页面数据
        QDD.data.my_index = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.my_index;

        // 初始化页面数据
        QDD.pageInit(data);
    });

    $(document).on("pageInit", "#nearby_video", function(e, id, page) {
        // 页面数据
        QDD.data.nearby_video = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.nearby_video;

        // 初始化页面数据
        QDD.pageInit(data);
    });
    $.init();
});