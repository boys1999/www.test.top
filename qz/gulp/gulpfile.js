var gulp = require('gulp'),
    sass = require('gulp-sass'),
    gulpConcat = require('gulp-concat'),
    fileInclude = require('gulp-file-include'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    replace = require('gulp-replace'),
    livereload = require('gulp-livereload');

// ==============================================开发配置
var versionLib="002";                                           //lib url 路径防止缓存
var versionStatic="003";                                        // static 路径防止缓存
var mod="weibo";                                                // 模块路径
var baseUrl="./../";                                            // 打包路径
// var baseUrl="D:/phpStudy/WWW/v3.palmst.top/public/qz";          // 打包路径
// ==============================================开发配置
// html,js,img
gulp.task('html_go', function() {
    gulp.src(['./../'+mod+'/*.html','./../'+mod+'/include/*.html'])
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(replace('?lib=001', '?lib='+versionLib))
        .pipe(replace('?static=001', '?static='+versionStatic))
        .pipe(gulp.dest(baseUrl+mod+'_build'));
    gulp.src('./../'+mod+'/static/js/**/*')
        .pipe(gulp.dest(baseUrl+mod+'_build/static/js'));
    gulp.src('./../'+mod+'/static/img/**/*')
        .pipe(gulp.dest(baseUrl+mod+'_build/static/img'));
});
gulp.task('html', function() {
    gulp.watch(['./../'+mod+'/**/*.html', './../'+mod+'/static/js/**/*', './../'+mod+'/static/img/**/*'], ['html_go']);
});

// sass
gulp.task('sass_go', function() {
    return gulp
        .src('./../'+mod+'/static/css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(baseUrl+mod+'_build/static/css'))
});
gulp.task('sass', function() {
    gulp.watch('./../'+mod+'/static/css/**/*.scss', ['sass_go']);
});

// .pipe(sass().on('error', sass.logError))

// 保存自动刷新
gulp.task('live', function() {
    livereload.listen();
    gulp.watch('./../'+mod+'_build/**/*.*', function(event) {
        livereload.changed(event.path);
    });
});

gulp.task('default', ['html', 'sass','live']);
// ==============================================打包配置