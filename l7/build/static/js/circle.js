$(function() {
    // ==============================================
    // 附近的人：列表
    $(document).on("pageInit", "#people", function(e, id, page) {
        // 页面数据
        QDD.data.people = {
            "title":"标题",
            "description":"描述",
            "shareTitle":"分享标题",
            "shareDescription":"分享描述",
            "shareLogo":"分享图片"
        };
        var data=QDD.data.people;

        // 初始化页面数据
        QDD.pageInit(data);

        // 组件： app
        data.app = {
            tipsLoad: {
                show:true,
                title:"加载中…",
                icon:"sory"
            },
            tipsEmpty: {
                show:false,
                title:"列表为空",
                icon:"sory"
            },
            items: [{
                text: 'Learn JavaScript'
            }, {
                text: 'Learn Vue'
            }, {
                text: 'Build something awesome'
            }]
        };
        var app = new Vue({
            el: '#app',
            data: data.app,
            mounted() {
                this.init()
            },
            methods:{
                init:function(){
                    setTimeout(function(){
                        data: data.app.tipsLoad.show=false;
                    },1000);
                }
            }
        });
    });
    // ==============================================
    // 附近的人：详情 （顶层类型页面）
    $(document).on("pageInit", "#people_detail", function(e, id, page) {
        $(page).css({"z-index":"3001" });//顶最顶层
    });

    $(document).on("pageInit", "#components", function(e, id, page) {
        // $(page).css({"z-index":"3001" });//顶最顶层
    });
    $.init();
});