var gulp = require('gulp'),
    sass = require('gulp-sass'),
    gulpConcat = require('gulp-concat'),
    fileInclude = require('gulp-file-include'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    livereload = require('gulp-livereload');

// ==============================================开发配置
// html,js,img
gulp.task('html_go', function() {
    gulp.src(['./../src/*.html'])
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./../build'));
    gulp.src('./../src/static/js/**/*')
        .pipe(gulp.dest('./../build/static/js'));
    gulp.src('./../src/static/img/**/*')
        .pipe(gulp.dest('./../build/static/img'));
});
gulp.task('html', function() {
    gulp.watch(['./../src/**/*.html', './../src/static/js/**/*', './../src/static/img/**/*'], ['html_go']);
});

// sass
gulp.task('sass_go', function() {
    return gulp
        .src('./../src/static/css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./../build/static/css'))
});
gulp.task('sass', function() {
    gulp.watch('./../src/static/css/**/*.scss', ['sass_go']);
});

// .pipe(sass().on('error', sass.logError))

// 保存自动刷新
gulp.task('live', function() {
    livereload.listen();
    gulp.watch('./../build/**/*.*', function(event) {
        livereload.changed(event.path);
    });
});

gulp.task('default', ['html', 'sass','live']);
// ==============================================打包配置