$(function() {
    $(document).on("pageInit", "#index", function(e, id, page) {
        // $(page).css({"z-index":"3001" });//顶最顶层
    });
    // 页面数据
    QDD.data.index = {
        "title":"标题",
        "description":"描述",
        "shareTitle":"分享标题",
        "shareDescription":"分享描述",
        "shareLogo":"分享图片"
    };
    var data=QDD.data.index;

    // 初始化页面数据
    QDD.pageInit(data);
    $.init();
});