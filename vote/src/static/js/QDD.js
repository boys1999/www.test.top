var QDD=(function(){

    // http 请求
    var http = function(obj, callBack) {
        $.ajax(obj).done(function(data) {
            callBack(data);
        }).fail(function() {
            console.log("error：");
            console.log(obj);
        }).always(function() {

        });
    };

    // 所有页面数据
    var data={};

    // 初始化一个页面数据
    var pageInit=function(pageData){
        var options={
            "title":"趣嗒嗒",
            "description":"趣嗒嗒科技",
            "shareTitle":"趣嗒嗒",
            "shareDescription":"趣嗒嗒科技",
            "shareLogo":"http://v3.palmst.cn/static/common/images/logo.jpg"
        };
        options=$.extend(options,pageData);
        //页面信息设置
        document.title=options.title;
        $('meta[name=description]').attr('content', options.description);
        //微信分享设置
    };

    return {
        "http":http,
        "data":data,
        "pageInit":pageInit
    }

})();